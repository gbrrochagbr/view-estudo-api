import React from "react";
import { withStyles } from "@material-ui/styles";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const styles = theme => ({
  appContainer: {
    width: "100%",
    minHeight: "100vh",
    background: "#222",
    color:'#fff',
    padding:20,
    boxSizing:'border-box',
    display:'flex',
    flexDirection:'column'
  },
  topBar:{
    display:'flex',
    textAlign:'center',
    borderRadius:6,
    overflow:'hidden'
  },
  button:{
    flex:1,
    color:'#fff',
    textDecoration:'none',
    backgroundColor:'#333',
    padding:'20px 0'
  },
  content:{
    flexGrow:1
  }
});


class App extends React.Component {

  

  render(){
    const { classes } = this.props;

    return (
      <Router>
        <div className={classes.appContainer}>
          <div className={classes.topBar}>
            <Link to="/" className={classes.button}>
              HOME
            </Link>
            <Link to="/sobre" className={classes.button}>
              SOBRE
            </Link>
          </div>
          {/* 
          <Route path="/sobre" component={Sobre} />
          <Route exact path="/" component={Teste} /> 
          */}
          <div className={classes.content}>
          
          </div>
        </div>
      </Router>
    );
  }
};

export default withStyles(styles)(App);
